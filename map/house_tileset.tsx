<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="house_tileset" tilewidth="8" tileheight="8" tilecount="256" columns="8">
 <transformations hflip="1" vflip="1" rotate="0" preferuntransformed="1"/>
 <image source="tiles.png" width="64" height="256"/>
 <tile id="12" type="Collision"/>
 <tile id="13" type="Collision"/>
 <tile id="14" type="Collision"/>
 <tile id="15" type="Door"/>
</tileset>
