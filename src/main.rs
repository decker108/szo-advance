#![no_std]
#![no_main]
#![cfg_attr(test, feature(custom_test_frameworks))] // This is required to allow writing tests
#![cfg_attr(test, reexport_test_harness_main = "test_main")]
#![cfg_attr(test, test_runner(agb::test_runner::test_runner))]
#![allow(non_snake_case)]

extern crate alloc;
use agb::{
    display::{
        tiled::{RegularBackgroundSize, TileSetting, TiledMap},
        Priority,
    },
    fixnum::{Rect, Vector2D},
    input::{Button, ButtonController},
    println,
};
use alloc::{vec, vec::Vec};
use szo_advance::{Entity, GameState, Level, Obstacle};

mod tilemap {
    include!(concat!(env!("OUT_DIR"), "/tilemap.rs"));
}

use crate::tilemap::{HORIZONTAL_FLIP, VERTICAL_AND_HORIZONTAL_FLIP, VERTICAL_FLIP};

#[agb::entry]
fn main(mut gba: agb::Gba) -> ! {
    game_main(gba) // Use a separate function since agb::entry macro breaks rust-analyzer
}

fn game_main(mut gba: agb::Gba) -> ! {
    agb::include_background_gfx!(background_tiles, tiles => "map/tiles.png");
    let vblank = agb::interrupt::VBlank::get();

    let (gfx, mut vram) = gba.display.video.tiled0();
    vram.set_background_palettes(background_tiles::PALETTES);

    let objCtrl = gba.display.object.get_managed();

    let mut player = Entity::new(&objCtrl, Rect::new((0, 0).into(), (16, 16).into()));
    player.updatePosition(0, 0);

    let mut input = ButtonController::new();
    let mut currentLevelIdx = 0;
    let mut bg = gfx.background(
        Priority::P1,
        RegularBackgroundSize::Background32x32,
        background_tiles::tiles.tiles.format(),
    );
    let mut obstacles: Vec<Obstacle> = vec![];
    renderBackground(
        &mut bg,
        &mut vram,
        &background_tiles::tiles.tiles,
        &mut obstacles,
        currentLevelIdx,
    );
    bg.commit(&mut vram);
    bg.set_visible(true);

    // *tilemap::EXITS.len();
    let exits = vec![]; // TODO load from EXITS field in tilemap
    let level = Level::new(obstacles, exits);

    println!("Initial setup done, entering main loop");

    let mut frame: usize = 0;
    let mut gameState = GameState::InGame;

    loop {
        vblank.wait_for_vblank();
        input.update(); // call update every loop

        let (xDiff, yDiff) = (input.x_tri() as i32, input.y_tri() as i32);
        let (newX, newY) = handleEntityMovement(
            &player,
            xDiff * player.movementSpeed,
            yDiff * player.movementSpeed,
            &level,
        );
        player.updatePosition(newX, newY);

        if input.is_just_pressed(Button::A) {
            println!("Button A was just pressed");
            player.useEquippedItem();
        }
        if input.is_just_pressed(Button::B) {
            println!("Button B was just pressed");
            player.interactWithFacedObject(&level);
        }
        if input.is_just_pressed(Button::L) {
            println!("Button L was just pressed");
            match gameState {
                GameState::InGame => {
                    player.openActiveItemSwitcher();
                }
                GameState::MainMenu | GameState::Menu => todo!(),
            }
        }
        if input.is_just_pressed(Button::R) {
            println!("Button R was just pressed");
            match gameState {
                GameState::InGame => {
                    player.openPassiveItemSwitcher();
                }
                GameState::MainMenu | GameState::Menu => todo!(),
            }
        }
        if input.is_just_pressed(Button::SELECT) {
            println!("Button SELECT was just pressed");
        }
        if input.is_just_pressed(Button::START) {
            println!("Button START was just pressed");
            match gameState {
                GameState::InGame => {
                    gameState = GameState::Menu;
                }
                GameState::Menu => {
                    gameState = GameState::InGame;
                }
                GameState::MainMenu => todo!(),
            }
        }

        player.handleAnimation((xDiff, yDiff), frame, &objCtrl);

        // draw sprites
        objCtrl.commit();

        frame = frame.wrapping_add(1);

        // syscall::halt();
    }
}

fn renderBackground(
    bg: &mut agb::display::tiled::MapLoan<'_, agb::display::tiled::RegularMap>,
    vram: &mut agb::display::tiled::VRamManager,
    tileset: &agb::display::tiled::TileSet<'_>,
    obstacles: &mut Vec<Obstacle>,
    level: u16,
) {
    const TILES_PER_LEVEL: u16 = 600;
    let mut obstacleId = 1;
    for y in 0..tilemap::HEIGHT as u16 {
        // 20 x 8px = 160px (see const display.HEIGHT)
        for x in 0..tilemap::WIDTH as u16 {
            // 30 * 8px = 240px (see const display.WIDTH)
            let tileId = *tilemap::DEFAULT_MAP
                .get((level * TILES_PER_LEVEL + x + tilemap::WIDTH as u16 * y) as usize)
                .unwrap_or(&(0, 0));
            bg.set_tile(
                vram,
                (x, y),
                &tileset,
                TileSetting::new(
                    tileId.0,
                    tileId.1 == VERTICAL_AND_HORIZONTAL_FLIP || tileId.1 == HORIZONTAL_FLIP,
                    tileId.1 == VERTICAL_AND_HORIZONTAL_FLIP || tileId.1 == VERTICAL_FLIP,
                    0,
                ),
            );
            if tileId.0 == 12 || tileId.0 == 13 || tileId.0 == 14 {
                obstacles.push(Obstacle::new(
                    obstacleId,
                    (x * 8, y * 8).into(),
                    Rect {
                        position: (x * 8, y * 8).into(),
                        size: (8, 8).into(),
                    },
                ));
                println!("Placing obstacle {} at position {},{}", obstacleId, x, y);
                obstacleId += 1;
            }
        }
    }
}

fn handleEntityMovement(entity: &Entity, xDiff: i32, yDiff: i32, level: &Level) -> (i32, i32) {
    let x;
    let y;
    let mut hasCollided = false;
    // TODO split into hasCollidedX and hasCollidedY to allow sliding along obstacle
    let potentialNewPos: Rect<i32> = Rect {
        position: (entity.position.x + xDiff, entity.position.y + yDiff).into(),
        size: (16, 16).into(),
    };
    for o in &level.obstacles {
        if o.collisionBox.touches(potentialNewPos) {
            hasCollided = true;
            break;
        }
    }
    if hasCollided {
        x = entity.position.x.clamp(8, agb::display::WIDTH - 24);
        y = entity.position.y.clamp(8, agb::display::HEIGHT - 24);
    } else {
        x = (entity.position.x + xDiff).clamp(8, agb::display::WIDTH - 24);
        y = (entity.position.y + yDiff).clamp(8, agb::display::HEIGHT - 24);
    }
    return (x, y);
}
