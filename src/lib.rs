#![no_std]
#![no_main]
#![cfg_attr(test, feature(custom_test_frameworks))] // This is required to allow writing tests
#![cfg_attr(test, reexport_test_harness_main = "test_main")]
#![cfg_attr(test, test_runner(agb::test_runner::test_runner))]
#![allow(non_snake_case)]

extern crate alloc;
use agb::{
    display::{
        object::{Graphics, OamManaged, Object, Tag, TagMap},
        Priority,
    },
    fixnum::{Rect, Vector2D},
};
use alloc::{vec, vec::Vec};

const SPRITESHEET: &Graphics = agb::include_aseprite!("gfx/main_spritesheet.aseprite");
const TAGS: &TagMap = SPRITESHEET.tags();
pub const ANIM_TAG_PLAYER_IDLE_UP: &Tag = TAGS.get("p_idle_up");
pub const ANIM_TAG_PLAYER_IDLE_DOWN: &Tag = TAGS.get("p_idle");
pub const ANIM_TAG_PLAYER_IDLE_HORIZ: &Tag = TAGS.get("p_idle_horiz");
pub const ANIM_TAG_PLAYER_WALK_UP: &Tag = TAGS.get("p_walk_up");
pub const ANIM_TAG_PLAYER_WALK_DOWN: &Tag = TAGS.get("p_walk_down");
pub const ANIM_TAG_PLAYER_WALK_HORIZ: &Tag = TAGS.get("p_walk_hor");

#[derive(Debug, PartialEq, Eq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub enum GameState {
    InGame,
    MainMenu,
    Menu,
}

pub struct Entity<'a> {
    sprite: Object<'a>,
    pub position: Vector2D<i32>,
    pub movementSpeed: i32,
    pub collisionBox: Rect<i32>,
    visible: bool,
    spriteOffset: usize,
    lastDirection: Direction,
}

impl<'a> Entity<'a> {
    pub fn new(object_controller: &'a OamManaged, collisionBox: Rect<i32>) -> Self {
        let mut sprite = object_controller.object_sprite(ANIM_TAG_PLAYER_IDLE_DOWN.sprite(0));
        let initialPosition = Vector2D { x: 0, y: 0 };
        sprite.set_priority(Priority::P0); // Sprite must have lower prio than background, or it will be rendered behind it.
        sprite.set_position(initialPosition);
        Entity {
            sprite,
            collisionBox,
            position: initialPosition,
            movementSpeed: 2,
            visible: true,
            spriteOffset: 0,
            lastDirection: Direction::Down,
        }
    }

    pub fn updatePosition(&mut self, x: i32, y: i32) {
        self.position.x = x as i32;
        self.position.y = y as i32;
        self.sprite.set_position(self.position);
        self.collisionBox.position = self.position;
    }

    pub fn updateSprite(&mut self, objCtrl: &'a OamManaged, flips: (bool, bool), animTag: &Tag) {
        self.sprite
            .set_sprite(objCtrl.sprite(animTag.sprite(self.spriteOffset)))
            .set_hflip(flips.0)
            .set_vflip(flips.1)
            .set_position(self.position)
            .show();
    }

    pub fn handleAnimation(
        &mut self,
        (xDiff, yDiff): (i32, i32),
        frame: usize,
        objCtrl: &'a OamManaged,
    ) {
        let isIdle = xDiff == 0 && yDiff == 0;
        if frame % 15 == 0 && isIdle {
            if self.spriteOffset == 0 {
                self.spriteOffset = 1;
            } else {
                self.spriteOffset = 0;
            }
            let idleAnimTag = match self.lastDirection {
                Direction::Down => ANIM_TAG_PLAYER_IDLE_DOWN,
                Direction::Up => ANIM_TAG_PLAYER_IDLE_UP,
                Direction::Left | Direction::Right => ANIM_TAG_PLAYER_IDLE_HORIZ,
            };
            self.updateSprite(
                objCtrl,
                (self.lastDirection == Direction::Left, false),
                idleAnimTag,
            );
        } else if frame % 6 == 0 && !isIdle {
            self.spriteOffset = incOrWrap(self.spriteOffset, 3);
            if yDiff == 0 {
                // player walking horizontally only
                self.updateSprite(objCtrl, (xDiff == -1, false), ANIM_TAG_PLAYER_WALK_HORIZ);
                self.lastDirection = if xDiff == -1 {
                    Direction::Left
                } else {
                    Direction::Right
                };
            } else if yDiff == -1 {
                // player walking up
                self.updateSprite(objCtrl, (false, false), ANIM_TAG_PLAYER_WALK_UP);
                self.lastDirection = Direction::Up;
            } else {
                // player walking down
                self.updateSprite(objCtrl, (false, false), ANIM_TAG_PLAYER_WALK_DOWN);
                self.lastDirection = Direction::Down;
            }
        }
    }

    pub fn useEquippedItem(&self) -> () {
        todo!() // TODO implement me
    }

    pub fn interactWithFacedObject(&self, level: &Level) -> () {
        todo!() // TODO implement me
    }

    pub fn openActiveItemSwitcher(&self) -> () {
        todo!()
    }

    pub fn openPassiveItemSwitcher(&self) -> () {
        todo!()
    }
}

pub struct Obstacle {
    pub id: i32,
    pub position: Vector2D<i32>,
    pub collisionBox: Rect<i32>,
}

impl Obstacle {
    pub fn new(id: i32, position: Vector2D<i32>, collisionBox: Rect<i32>) -> Self {
        Obstacle {
            id,
            position,
            collisionBox,
        }
    }
}

pub struct Exit {
    pub position: Vector2D<i32>,
    pub size: Vector2D<i32>,
    pub transitionTo: u8,
}

impl Exit {
    pub fn new(position: Vector2D<i32>, size: Vector2D<i32>, transitionTo: u8) -> Self {
        Self {
            position,
            size,
            transitionTo,
        }
    }
}

pub struct Level {
    pub obstacles: Vec<Obstacle>,
    pub exits: Vec<Exit>,
}

impl Level {
    pub fn new(obstacles: Vec<Obstacle>, exits: Vec<Exit>) -> Self {
        Level { obstacles, exits }
    }
}

pub fn incOrWrap(input: usize, limit: usize) -> usize {
    return if input + 1 < limit { input + 1 } else { 0 };
}
