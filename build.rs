#![allow(non_snake_case)]
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use tiled::{parse_file, LayerData, PropertyValue}; // TODO consider upgrading to 0.11.x

// Work-around to allow console output from build script.
macro_rules! p {
    ($($tokens: tt)*) => {
        println!("cargo:warning={}", format!($($tokens)*))
    }
}

const VERTICAL_AND_HORIZONTAL_FLIP: u16 = 3;
const VERTICAL_FLIP: u16 = 2;
const HORIZONTAL_FLIP: u16 = 1;
const NO_FLIP: u16 = 0;

const LEVEL_NAMES: &[&str] = &["house_map", "level2"];

#[derive(Clone)]
struct Pair(u16, u16);

// This looks ridiculous only because the author of the quote crate is being unreasonable and difficult...
impl ToTokens for Pair {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let first = self.0;
        let second = self.1;
        tokens.extend(quote! {
            (#first, #second)
        });
    }
}

fn main() {
    p!("Running build script");

    // This env var is set to the target folder by default
    let out_dir = env::var("OUT_DIR").expect("OUT_DIR environment variable must be specified");
    p!("Out dir is: {}", out_dir);

    // Specify the name of your tile map file here (tmx file).
    let house_map_filename = "map/house_map.tmx";
    println!("cargo:rerun-if-changed={house_map_filename}");

    p!("Levels to process: {}", LEVEL_NAMES.len());

    let mut maps = vec![];
    let mut tiles = vec![];
    let mut exits = vec![];
    for filename in LEVEL_NAMES {
        p!("Processing level: {}", filename);
        let map = parse_file(Path::new(&format!("map/{filename}.tmx"))).unwrap();
        // For simplicity's sake, we use a single-layer tile map.
        p!(
            "Map {} has {} layers, {} object groups",
            filename,
            &map.layers.len(),
            &map.object_groups.len()
        );
        let default_layer = &map.layers[0]; // TODO implement support for foreground and UI

        // Extract the tile ids. These will match the indexes of the tiles in the .tsx tile set file (or its backing image file).
        let default_tiles = extract_tiles(&default_layer.tiles);

        // TODO add level index to exit objects
        let exitObjs = extract_exits(&map.object_groups[0]);

        exits.extend(exitObjs);
        maps.push(map);
        tiles.push(default_tiles);
    }
    let default_tiles: Vec<Pair> = tiles.concat();

    // The width and height for a GBA screen is 30x20 assuming 8px tiles,
    // and all our levels are identical in size.
    let width = maps[0].width;
    let height = maps[0].height;

    p!(
        "Total tile count in tileset: {:?}",
        maps[0].tilesets[0].tilecount.or(Some(0))
    );
    p!(
        "Total tiles with classes in tileset: {:?}",
        maps[0].tilesets[0].tiles.len()
    );

    // Generate Rust code containing our tile map data. This will be a one-dimensional array of
    // tile indexes positioned according to the tile map, as well as some consts with metadata.
    let output = quote! {
        use szo_advance::Exit;
        use agb::fixnum::Vector2D;
        pub const DEFAULT_MAP: &[(u16, u16)] = &[#(#default_tiles),*];

        pub const WIDTH: i32 = #width as i32;
        pub const HEIGHT: i32 = #height as i32;

        pub const VERTICAL_AND_HORIZONTAL_FLIP: u16 = 3;
        pub const VERTICAL_FLIP: u16 = 2;
        pub const HORIZONTAL_FLIP: u16 = 1;
        pub const NO_FLIP: u16 = 0;

        pub const EXITS: &[Exit] = &[#(#exits),*];
    };

    // Print the above string to a file named tilemap.rs that can be imported from our main Rust program.
    let output_file = File::create(format!("{out_dir}/tilemap.rs"))
        .expect("failed to open tilemap.rs file for writing");
    let mut writer = BufWriter::new(output_file);
    write!(&mut writer, "{output}").unwrap();

    p!("Done running build script");
}

fn extract_exits(objectGroups: &tiled::ObjectGroup) -> Vec<TokenStream> {
    p!(
        "Object group '{}' has {} objects",
        objectGroups.name,
        objectGroups.objects.len()
    );
    let exitObjects = objectGroups.objects.iter()
    .filter(|obj| obj.obj_type == "DoorTrigger")
    .map(|obj| {
        p!(
            "{}: type={} x,y={},{}, w,h={},{}, props={:?}",
            obj.name,
            obj.obj_type,
            obj.x,
            obj.y,
            obj.width,
            obj.height,
            obj.properties
        );
        let transitionTo = extractIntPropertyValue(obj, "transition_to") as u8;
        (obj.x.floor() as i32, obj.y.floor() as i32, obj.width.floor() as i32, obj.height.floor() as i32, transitionTo)
    })
    .map(|(x,y,width,height,transition)| quote!(Exit{position: Vector2D{x: #x, y: #y}, size: Vector2D{x: #width, y: #height}, transitionTo: #transition}))
    .collect::<Vec<_>>();
    return exitObjects;
}

fn extractIntPropertyValue(obj: &tiled::Object, parameterName: &str) -> i32 {
    // This destructuring of an enum with a value initializes the transition variable with the value of the property.
    if let PropertyValue::IntValue(value) = obj
        .properties
        .get(parameterName)
        .unwrap_or(&PropertyValue::IntValue(0))
    {
        value.to_owned()
    } else {
        0
    }
}

fn extract_tiles(layer: &'_ LayerData) -> Vec<Pair> {
    match layer {
        LayerData::Finite(tiles) => tiles.iter().flat_map(|row| {
            row.iter().map(|tile| {
                let flip = if tile.flip_h && tile.flip_v {
                    VERTICAL_AND_HORIZONTAL_FLIP
                } else if tile.flip_h {
                    HORIZONTAL_FLIP
                } else if tile.flip_v {
                    VERTICAL_FLIP
                } else {
                    NO_FLIP
                };
                (tile.gid, flip)
            })
        }),
        _ => unimplemented!("cannot use infinite layer"),
    }
    .map(get_map_id)
    .map(|x| Pair(x.0, x.1))
    .collect::<Vec<_>>()
}

fn get_map_id((tileId, flipMode): (u32, u16)) -> (u16, u16) {
    match tileId {
        0 => (0, 0),
        i => (i as u16 - 1, flipMode),
    }
}
